'use strict'

import 'mocha';
import chai from 'chai';
import chaiHttp from 'chai-http';
import app from '../index';

chai.use(chaiHttp);

const expect = chai.expect;

describe('Test cases:', ()=> {

    describe('repos:', ()=> {
        describe('GET /repos/:owner/:repo:', ()=> {
            it('Should return 400 and error message if page number is empty on /repos/:owner/:repo GET', (done)=> {
                const page = '';
                const limit = 2;
                const owner = 'fluture-js';
                const repo = 'Fluture';
                chai.request(app)
                    .get(`/repos/${owner}/${repo}?page=${page}&limit=${limit}`)
                    .end((err, res) => {
                        if (err) {
                            throw err;
                        }
                        expect(res).to.have.status(400);
                        expect(res).to.be.a('object');
                        expect(res.body).to.have.property('error');
                        expect(res.body.error).to.be.true;
                        expect(res.body).to.have.property('message');
                        expect(res.body.message).to.have.lengthOf.at.least(1);
                        expect(res.body.message).to.equal('Page is required');
                        done();
                });
            });

            it('Should return 400 and error message if page number is non numeric on /repos/:owner/:repo GET', (done)=> {
                const page = 'a';
                const limit = 2;
                const owner = 'fluture-js';
                const repo = 'Fluture';
                chai.request(app)
                    .get(`/repos/${owner}/${repo}?page=${page}&limit=${limit}`)
                    .end((err, res) => {
                        if (err) {
                            throw err;
                        }
                        expect(res).to.have.status(400);
                        expect(res).to.be.a('object');
                        expect(res.body).to.have.property('error');
                        expect(res.body.error).to.be.true;
                        expect(res.body).to.have.property('message');
                        expect(res.body.message).to.have.lengthOf.at.least(1);
                        expect(res.body.message).to.equal('Invalid page');
                        done();
                });
            });

            it('Should return 400 and error message if limit is empty on /repos/:owner/:repo GET', (done)=> {
                const page = 1;
                const limit = '';
                const owner = 'fluture-js';
                const repo = 'Fluture';
                chai.request(app)
                    .get(`/repos/${owner}/${repo}?page=${page}&limit=${limit}`)
                    .end((err, res) => {
                        if (err) {
                            throw err;
                        }
                        expect(res).to.have.status(400);
                        expect(res).to.be.a('object');
                        expect(res.body).to.have.property('error');
                        expect(res.body.error).to.be.true;
                        expect(res.body).to.have.property('message');
                        expect(res.body.message).to.have.lengthOf.at.least(1);
                        expect(res.body.message).to.equal('Limit is required');
                        done();
                });
            });

            it('Should return 400 and error message if limit is non numeric on /repos/:owner/:repo GET', (done)=> {
                const page = 1;
                const limit = 'a';
                const owner = 'fluture-js';
                const repo = 'Fluture';
                chai.request(app)
                    .get(`/repos/${owner}/${repo}?page=${page}&limit=${limit}`)
                    .end((err, res) => {
                        if (err) {
                            throw err;
                        }
                        expect(res).to.have.status(400);
                        expect(res).to.be.a('object');
                        expect(res.body).to.have.property('error');
                        expect(res.body.error).to.be.true;
                        expect(res.body).to.have.property('message');
                        expect(res.body.message).to.have.lengthOf.at.least(1);
                        expect(res.body.message).to.equal('Invalid limit');
                        done();
                });
            });

            it('Should return 422 and error message if requested repo is not found on /repos/:owner/:repo GET', (done)=> {
                const page = 1;
                const limit = 2;
                const owner = 'qb-soorajlal';
                const repo = 'test-not-found';
                chai.request(app)
                    .get(`/repos/${owner}/${repo}?page=${page}&limit=${limit}`)
                    .end((err, res) => {
                        if (err) {
                            throw err;
                        }
                        expect(res).to.have.status(422);
                        expect(res).to.be.a('object');
                        expect(res.body).to.have.property('error');
                        expect(res.body.error).to.be.true;
                        expect(res.body).to.have.property('message');
                        expect(res.body.message).to.have.lengthOf.at.least(1);
                        expect(res.body.message).to.equal('Requested repo is not found or inaccessible');
                        done();
                });
            });

            it('Should return 422 and error message if requested repo is private on /repos/:owner/:repo GET', (done)=> {
                const page = 1;
                const limit = 2;
                const owner = 'qb-soorajlal';
                const repo = 'test-repo';
                chai.request(app)
                    .get(`/repos/${owner}/${repo}?page=${page}&limit=${limit}`)
                    .end((err, res) => {
                        if (err) {
                            throw err;
                        }
                        expect(res).to.have.status(422);
                        expect(res).to.be.a('object');
                        expect(res.body).to.have.property('error');
                        expect(res.body.error).to.be.true;
                        expect(res.body).to.have.property('message');
                        expect(res.body.message).to.have.lengthOf.at.least(1);
                        expect(res.body.message).to.equal('Requested repo is not found or inaccessible');
                        done();
                });
            });

            it('Should return 200 and commits of default branch if valid page number and limit are provided on /repos/:owner/:repo GET', (done)=> {
                const page = 1;
                const limit = 2;
                const owner = 'fluture-js';
                const repo = 'Fluture';
                chai.request(app)
                    .get(`/repos/${owner}/${repo}?page=${page}&limit=${limit}`)
                    .end((err, res) => {
                        if (err) {
                            throw err;
                        }
                        expect(res).to.have.status(200);
                        expect(res).to.be.a('object');
                        expect(res.body).to.have.property('error');
                        expect(res.body.error).to.be.false;
                        expect(res.body).to.have.property('data');
                        expect(res.body.data).to.be.a('object');
                        expect(res.body.data).to.have.property('page');
                        expect(res.body.data.page).to.be.at.least(1);
                        expect(res.body.data).to.have.property('per_page');
                        expect(res.body.data.per_page).to.be.at.least(1);
                        expect(res.body.data).to.have.property('total_pages');
                        expect(res.body.data.total_pages).to.be.at.least(1);
                        expect(res.body.data).to.have.property('total_items');
                        expect(res.body.data.total_items).to.be.at.least(1);
                        expect(res.body.data).to.have.property('commits');
                        expect(res.body.data.commits).to.be.an('array');
                        expect(res.body.data.commits[0].shaid).to.exist;
                        expect(res.body.data.commits[0].committer).to.exist;
                        expect(res.body.data.commits[0].message).to.exist;
                        expect(res.body.data.commits[0].date).to.exist;
                        done();
                });
            });

            it('Should return 200 and commits if branch name, valid page number and limit are provided on /repos/:owner/:repo GET', (done)=> {
                const page = 1;
                const limit = 2;
                const owner = 'fluture-js';
                const repo = 'Fluture';
                const branch = 'avaq/c8-oletus';
                chai.request(app)
                    .get(`/repos/${owner}/${repo}?page=${page}&limit=${limit}&branch=${branch}`)
                    .end((err, res) => {
                        if (err) {
                            throw err;
                        }
                        expect(res).to.have.status(200);
                        expect(res).to.be.a('object');
                        expect(res.body).to.have.property('error');
                        expect(res.body.error).to.be.false;
                        expect(res.body).to.have.property('data');
                        expect(res.body.data).to.be.a('object');
                        expect(res.body.data).to.have.property('page');
                        expect(res.body.data.page).to.be.at.least(1);
                        expect(res.body.data).to.have.property('per_page');
                        expect(res.body.data.per_page).to.be.at.least(1);
                        expect(res.body.data).to.have.property('total_pages');
                        expect(res.body.data.total_pages).to.be.at.least(1);
                        expect(res.body.data).to.have.property('total_items');
                        expect(res.body.data.total_items).to.be.at.least(1);
                        expect(res.body.data).to.have.property('commits');
                        expect(res.body.data.commits).to.be.an('array');
                        expect(res.body.data.commits[0].shaid).to.exist;
                        expect(res.body.data.commits[0].committer).to.exist;
                        expect(res.body.data.commits[0].message).to.exist;
                        expect(res.body.data.commits[0].date).to.exist;
                        done();
                });
            });

        });
    });

    after((done) => {
        console.log('Done!!');
        done();
        process.exit(); 
    });
});
