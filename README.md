# Github Express App
An application that with
  - Github public repo commit list API

### Requirements
  - Node.js v13
  - Redis

### Installation
Install the dependencies and devDependencies and start the server.

```sh
$ cd github-express-app
$ npm install
$ npm run build
$ npm start
```

To run lint,
```sh
$ npm run lint
```

To run test cases,
```sh
$ npm run test
```

### Configuration
  - Copy .env.example to .env.
  - Open .env file in any editor and set your host information and GitHub API token.
