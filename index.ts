'use strict'

import express from 'express';
import dotenv from 'dotenv';
import cors from 'cors';
import indexRouter from './app/routers/indexRouter';
import reposRouter from './app/routers/reposRouter';

dotenv.config();

const app = express();
const port = process.env.PORT || 8080;

app.use(cors());
app.use('/', indexRouter);
app.use('/repos', reposRouter);

//handle 404
app.use((req, res)=>{
    res.status(404).json({
        error: true,
        message: 'Not found!!'
    })
});

app.listen(port, ()=>{
    console.log(`listening to port ${port}`);
})

export default app;
