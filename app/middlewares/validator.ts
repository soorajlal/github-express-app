'use strict'

import { check, validationResult } from 'express-validator';
import * as R from 'ramda';

export const reposGetAllValidationRules = () => {
    return [ 
        check('page').trim().notEmpty().withMessage('Page is required').isNumeric().withMessage('Invalid page'),
        check('limit').trim().notEmpty().withMessage('Limit is required').isNumeric().withMessage('Invalid limit')
    ]
}

export const validate = (req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        //get first error
        const condtn = R.pipe(R.pluck('msg'), R.head);
        const message = condtn(errors.array());
    
        return res.status(400).json({
            error: true,
            message: message
        });
    }
    next();
}
