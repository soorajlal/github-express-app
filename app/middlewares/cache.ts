'use strict'

import client from '../config/config'


export const commitsCache = (req, res, next) => {
	const {page, limit, branch} = req.query;
    const {owner, repo} = req.params;
    
    const commitsRedisKey = (branch)? `commits:${owner}:${repo}:${page}:${limit}:${branch}` : `commits:${owner}:${repo}:${page}:${limit}`;
    
    client.get(commitsRedisKey,  (err, result)=> {
        if (err) {
        	return res.status(500).json({
                error: true,
                message: 'Unable to process the request'
            });
        }

        if (result != null) {
        	const data = JSON.parse(result);

            return res.status(200).json({
                error: false,
                data: data
            });
        } else {
            next();
        }
    });
}
