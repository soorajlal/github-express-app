'use strict'

import {attemptP, fork, map} from 'fluture'
import * as R from 'ramda';
import got from 'got'
import client from '../config/config'

export const getCommits = (req, res)=>{
    const {page, limit, branch} = req.query;
    const {owner, repo} = req.params;
    const apiBaseUrl = `https://api.github.com/repos/${owner}/${repo}`;
    const firstPageUrl = (branch)? `${apiBaseUrl}/commits?page=1&per_page=1&sha=${branch}` : `${apiBaseUrl}/commits?page=1&per_page=1`;
    const pageUrl = (branch)? `${apiBaseUrl}/commits?page=${page}&per_page=${limit}&sha=${branch}` : `${apiBaseUrl}/commits?page=${page}&per_page=${limit}`;
    
    const getGithubAPI = url => (
        attemptP (() => got(url, {'headers':{'authorization':`token ${process.env.GITHUB_TOKEN}`}}) )
    );

    const getGithubCommitsCount = getGithubAPI (firstPageUrl)
                      .pipe (map (reqst => reqst.headers.link))

    const getGithubCommits = getGithubAPI (pageUrl)
                      .pipe (map (reqst => JSON.parse(reqst.body)))

    //get total commits count
    getGithubCommitsCount.pipe (fork (()=>{ 
        return res.status(422).json({
            error: true,
            message: 'Requested repo is not found or inaccessible'
        });

    }) (tresult => { 

        //extract total commits count from headers
        const link = <string>tresult;
        const spiltLinkCondtn = R.pipe(R.split(','), R.last, R.split(';'), R.head, R.split('<'), R.last, R.split('>'), R.head);
        const lastPageUrl = spiltLinkCondtn(link);
        const spiltUrlCondtn = R.pipe(R.split('?'), R.last, R.split('&'), R.head, R.split('='), R.last, Number);
        const totalItems = spiltUrlCondtn(lastPageUrl);

        //get github commits
        getGithubCommits.pipe (fork (()=>{ 
            
            return res.status(422).json({
                error: true,
                message: 'Requested repo is not found or inaccessible'
            });

        }) (cresult => { 
            
            const commits = cresult.map(entry => {
                return {
                    shaid: entry.sha,
                    committer: entry.commit.committer.name,
                    message: entry.commit.message,
                    date: entry.commit.committer.date
                }
            });
            
            //get total pages and count
            const currentPage = Number(page);
            const perPage = Number(limit);
            const totalPages = Math.ceil(totalItems/perPage);

            const data = {
                page: currentPage,
                per_page: perPage,
                total_pages: totalPages,
                total_items: totalItems,
                commits: commits
            };

            const redis_expiry = Number(process.env.REDIS_EXPIRY) || 300; // seconds
            const commitsRedisKey = (branch)? `commits:${owner}:${repo}:${currentPage}:${perPage}:${branch}` : `commits:${owner}:${repo}:${currentPage}:${perPage}`;
            client.setex(commitsRedisKey, redis_expiry, JSON.stringify(data));

            return res.status(200).json({
                error: false,
                data: data
            });
        }));
    }));

};
