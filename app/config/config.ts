'use strict'

import redis from 'redis'

const redis_port = Number(process.env.REDIS_PORT) || 6379;

const client = redis.createClient(redis_port);

client.on('connect', () => {
    console.log(`connected to redis`);
});

client.on('error', err => {
    console.log(`Error: ${err}`);
});

export default client;
