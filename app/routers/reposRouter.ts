'use strict'

import {Router} from 'express';
import * as reposController from '../controllers/reposController'
import * as validator from '../middlewares/validator';
import * as cache from '../middlewares/cache';
const router = Router();

router.get('/:owner/:repo', validator.reposGetAllValidationRules(), validator.validate,cache.commitsCache, reposController.getCommits);

export default router;
